let randoom_margin = ["-5px", "1px", "5px", "7px", "10px"];
let random_color = ["#c2ff3d", "#ff3de8", "#3dc2ff", "#04e022", "#bc83e6", "#ebb328"];
let random_degree = ["rotate(3deg)", "rotate(1deg)", "rotate(-1deg)", "rotate(-3deg)", "rotate(-5deg)", "rotate(-8deg)"];

document.querySelector (".add-note-btn").addEventListener("click", () => {
    document.querySelector(".modal").style.display = "flex";
});
document.querySelector (".inner-modal i").addEventListener ("click", () => {
    document.querySelector(".modal").style.display = "none";
});

let ok = document.querySelector (".ok-btn");
ok.addEventListener ("click", () => {
    let note = document.createElement ("div");
    let details = document.createElement ("div");
    let noteText = document.createElement ("h3");
        
    note.setAttribute ("style", `margin: ${randoom_margin[Math.floor(Math.random() * randoom_margin.length)]}; background-color: ${random_color[Math.floor(Math.random() * random_color.length)]}; transform : ${random_degree[Math.floor(Math.random() * random_degree.length)]}`);
    note.classList.add ("note");
    details.classList.add ("details");

    details.appendChild (noteText);
    note.appendChild (details);
    
    let text = document.querySelector (".inner-modal textarea").value;
    noteText.innerText = text;
    
    saveLocalTodo (noteText.innerText)

    document.querySelector(".all-notes").appendChild(note);
    document.querySelector (".inner-modal textarea").value = "";
});


function saveLocalTodo (text) {
    let notes;

    if (localStorage.getItem ("notes") === null) {
        notes = [];
    } else {
        notes = JSON.parse(localStorage.getItem ("notes"));
    }
    notes.push(text);
    localStorage.setItem("notes", JSON.stringify (notes));
}